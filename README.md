# A Wordpress Reader

Features:
- 100% React code
- Compatible with any modern Wordpress site
- **[SOON]** PWA: Behaves as a native app (installable on mobile devices and desktop)
- **[SOON]** PWA: Enables saving selected articles for offline reading 
- Free to use and share!

Open source project. *Feel free to contribute.*
